/*can013, sto020*/

var modalMain = document.getElementById("movie-modal");
var mainWindow = document.getElementById("main-wrapper");
var close = document.getElementById("close");
var content_div = document.getElementById('modal-content');



function openModal()
{
    modalMain.style.display = "block";
}
function hideModal()
{
    modalMain.style.display = "none";

}
// showMovieModal = use httpObject to load in movie page and time it.
function showMovieModal(movie_id)
{

    var xmlHttp = new XMLHttpRequest();

    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
        {
            content_div.innerHTML = xmlHttp.responseText;
        }
    };

    xmlHttp.open("GET", movie_id, true); // true for asynchronous
    xmlHttp.send(null);

    //Open the modal window
    openModal();
}

