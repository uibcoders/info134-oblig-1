/**
 * Created by sto020, can013, jmo034 on 12.03.2017.
 */

/*Create posters from movies input.
* div - The content holder for output.
* movies - An array of all movie objects to create poster from.
*/
function createMoviePosters(div, movies) {
    div.innerHTML = "";
    
    var length = movies.length;
    if (length <= 0) {
        return;
    }

    console.info("Number of posters : " + length);
    

    var html = "";
    html = movies.map(movie => `
      <div class="movie-wrapper">
      <div class="movie-image">
      <a href="${getRootUrl()}/pages/movie.html?id=${movie.id}">
      <img class="img-loading movie-thumbnail-lg" onload="imageLoaded(this);" onerror="onImageError(this);" src="${getMoviePoster(movie.id)}" />
      </a>
      </div>
      <div class="poster-details">
      <div class="movie-detail white">${refactorTitle(movie.otitle)}</div>
      <div class="movie-detail white">${movie.year} </div>
      <div class="movie-detail white"> <a onclick="showModal(${movie.id}); false;">?</a> ${getMovieGenres(movie).map(genre => `<a href="${getRootUrl()}/pages/search.html?genre=${genre.trim()}">${genre}</a>`).join(", ")}</div>
      </div>
      </div>`).join("");
    
    div.innerHTML += html;

}

/*Create a list of movies, like search results.
* div - The content holder for output.
* movies - An array of movies to create thumbnails from.
*/
function createMovieThumbnails(div, movies) {
    div.innerHTML = "";

    console.info("Number of thumbnails : " + movies.length);

    var html = movies.map(movie => `
        <div class="thumbnail-wrapper">
        <a href="${getRootUrl()}/pages/movie.html?id=${movie.id}">
        <img class="img-loading movie-thumbnail-sm" onload="imageLoaded(this);" onerror="onImageError(this);" src="${getMoviePoster(movie.id)}" alt="${movie.otitle}"/></a>
        <a class="thumbnail-description" href="${getRootUrl()}/pages/movie.html?id=${movie.id}">${movie.otitle}</a>
        </div>
        `).join("");
    div.innerHTML = html;
}

/*Create a list of movies, like search results.
* div - The content holder for output.
* movies - An array of movies to create a list of.
*/
function createMovieList(div, movies) {
    div.innerHTML = "";
    
    var html = movies.map(movie =>
        `   
<div class="search-result">
    <table>
        <thead>
            <td colspan="3" align="left"><a href='${getRootUrl()}/pages/movie.html?id=${movie.id}'><h3 class="title white">${refactorTitle(movie.otitle)} (${movie.year})</h3></a></td>
        </thead>

        <tbody>
            <td style="min-width:90px; width:100px; vertical-align: top;">
                <div class="thumbnail-wrapper">
                    <a href="${getRootUrl()}/pages/movie.html?id=${movie.id}" title="${refactorTitle(movie.otitle)} - ${checkData(movie.year)}">
                        <img class="movie-thumbnail-md" onload="imageLoaded(this);" onerror="onImageError(this);"  src="${getMoviePoster(movie.id)}"/></a>
                </div>
                <div class="movie-detail black">Format here</div>
                <div class="movie-detail black">Rating: ${getAverageRating(movie)}</div>
            </td>
            <td>
                <div class="movie-detail">${getMovieGenres(movie).map(genre => `<a class="tag white" href="${getRootUrl()}/pages/search.html?genre=${genre.trim()}">${genre}</a>`).join(" ")}</div>
                <div style="" class="movie-info">
                    <p>${getMovieDescription(movie)}</p>
                </div>
            </td>
            <td>
                <div class="rotate"></div>
            </td>
        </tbody>
    </table>
</div>
 `).join("");

    div.innerHTML = html; //append results to results

}

/*Create movie page
* div - The container to put html output.
* movie_object - The movie to create page from.
*/
function createMoviePage(div,movie_object)
{
    var html = movie_object.map(movie => `
 
                <h1 class="page-heading">${refactorTitle(movie.otitle)}</h1>
                <div class="movie-info">
                    <div>
                        <div class="movie-image">
                            <img class="img-loading movie-thumbnail-xl" onerror="onImageError(this);" src="${getMoviePoster(movie.id)}" >
                            <fieldset class="score">
                                <legend>Score:</legend>
                                <h2>?What did you think about the movie</h2>

                                <input class="star" type="radio" id="score-5" name="score" value="5" />
                                <label title="5 stars" for="score-5">5 stars</label>

                                <input class="star" type="radio" id="score-4" name="score" value="4" />
                                <label title="4 stars" for="score-4">4 stars</label>

                                <input class="star" type="radio" id="score-3" name="score" value="3" />
                                <label title="3 stars" for="score-3">3 stars</label>

                                <input class="star" type="radio" id="score-2" name="score" value="2" />
                                <label title="2 stars" for="score-2">2 stars</label>

                                <input class="star" type="radio" id="score-1" name="score" value="1" />
                                <label title="1 stars" for="score-1">1 stars</label>

                            </fieldset>
                            <p class="movie-detail black"><strong>Vurdering: </strong>${getAverageRating(movie)}</p>
                            <div>
                            <a class="button button-big" href="#">+ Add to list</a>
                            <a href="${getMovieTrailer(movie)}" id="trailer-link" target="_blank" class="button button-trailer">Spill trailer</a>
                        </div>
                        </div>

                    </div>

                    <div class="">

                        <p class="movie-detail black"><strong>Lengde: </strong>${checkData(movie.length)} min</p>
                        <p class="movie-detail black"><strong>Norsk tittel: </strong>${checkData(movie.ntitle)}</p>

                        <p class="movie-detail black"><strong>Land: </strong>${checkData(movie.country).split(',').map(c => `<a  href="${getRootUrl()}/pages/search.html?country=${c.trim()}">${c}</a>&nbsp;`).join(",")}</p>
                        <p class="movie-detail black"><strong>År: </strong><a href="${getRootUrl()}/pages/search.html?year=${movie.year}">${movie.year}</a></p>
                        <p class="movie-detail"><strong>Kategori(er): </strong>${getMovieGenres(movie).map(genre => `<a class="tag white" href="${getRootUrl()}/pages/search.html?genre=${genre.trim()}">${genre}</a>&nbsp;`).join("")}</p>
                        <p class="movie-detail black"><strong>Regissør(er): </strong>${getMovieDirectors(movie).split(/[,/]/).map(director => `<a  href="${getRootUrl()}/pages/search.html?director=${director.trim()}">${director}</a>`).join(",&nbsp;")}</p>
                        <p class="movie-detail black"><strong>Skuespillere: </strong>${getMovieActors(movie).split(/[,/]/).map(star => `<a  href="${getRootUrl()}/pages/search.html?actor=${star.trim()}">${star}</a>&nbsp;`).join(",&nbsp;")}</p>

                        <a class="movie-detail black" href="#reviews"><strong>Kommentarer: </strong>${getMovieReviews(movie).length} har kommentert på filmen.</a>

                     

                    </div>
                </div>

                <div class="clearfix"></div>

                <h2 class="page-heading">Beskrivelse</h2>

                <p class="movie-description">${getMovieDescription(movie)} </p>

        <div>

            <div id="reviews" class="content">
             <p class="movie-detail black"><strong>Reviews: </strong>${getMovieReviews(movie).map(review => `
<span><p>Modified: ${review.mod_date} Username: ${review.username} Rating: ${review.rating} <p><h3>Comment: </h3>${review.comment}</p></p></span>&nbsp;
`).join("")}</p>
            </div>

        </div>
`).join("");
    
    div.innerHTML = html;
    
}


/*Create a modal view
* div - The content holder for output.
* movie - An array of movies to create a modal of.
*/
function createMovieModal(div, movies) {
    div.innerHTML = "";
    var html = movies.map(movie =>
        `   
<div class="">
    <table>
        <thead>
            <td colspan="3" align="left"><a href='${getRootUrl()}/pages/movie.html?id=${movie.id}'><h3 class="title white">${refactorTitle(movie.otitle)} (${movie.year})</h3></a></td>
        </thead>

        <tbody>
            <td style="min-width:90px; width:100px; vertical-align: top;">
                <div class="thumbnail-wrapper">
                    <a href="${getRootUrl()}/pages/movie.html?id=${movie.id}" title="${refactorTitle(movie.otitle)} - ${checkData(movie.year)}">
                        <img class="movie-thumbnail-md" onload="imageLoaded(this);" onerror="onImageError(this);"  src="${getMoviePoster(movie.id)}"/></a>
                </div>
                <div class="movie-detail black">Format here</div>
                <div class="movie-detail black">Rating: NaN</div>
            </td>
            <td>
                <div style="" class="movie-info">
                    <p class="white">${getMovieDescription(movie).substring(0,250)}...</p>
                </div>
            </td>
        </tbody>
    </table>
</div>
 `).join("");

    div.innerHTML = html; //append results to results

}