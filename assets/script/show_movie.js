var content = document.querySelector("#movie-content");


window.onload = function() {
    var query_params = get_query_string_parameters();

    if (!query_params.id) {
        content.innerHTML = '<h1>No id given</h1>';
        return;
    }

    show(movies_object[query_params.id]);

};



function show(movie_object) {
    var movie = [];
    if (!movie_object) {
        content.innerHTML = "<h1>Could not retrieve movie with id" + query_params.id + "!</h1>";
        return;
    }
    movie.push(movie_object);
    setPageTitle("Movie: " + movie[0].otitle);
    //Create html movie page
    createMoviePage(content, movie);


}