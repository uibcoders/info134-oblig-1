/* Created by Sto020, can013 */

var resultList = document.querySelector("#search-results");

var pages = [],
    currentPage,
    button_next = document.querySelector("#btn-next"),
    button_prev = document.querySelector("#btn-prev"),
    text_pages = document.querySelector("#text-pages");

/*Create arrays within a new array with a specified size.
* Used for pagination
* arr   - Input array.
* cSize - The sizes of the child arrays.
*/
function createPages(arr, cSize) {
    if (pages.length > 0) {return pages; }
    var i;
    for (i = 0; i < arr.length; i += cSize) {
        pages.push(arr.slice(i, i + cSize));
    }
    return pages;
}

//Creates and displays data on the page
//Uses currentPage as 
function displayPage() {
    setPageTitle("All movies - Page " + currentPage + " of " + pages.length);

    if (pages.length > 0 && (currentPage <= (pages.length)) && currentPage >= 1) {
        text_pages.innerHTML = "Page " + currentPage + " of " + pages.length;
        createMovieList(document.querySelector("#search-results"), pages[currentPage - 1]);
        //createMovieThumbnails(document.querySelector("#movies-main"),pages[currentPage-1]);
    }
       
}


//Goes to the next page 
function nextPage() {
    if (pages.length > 0 && (currentPage >= (pages.length))) {return; }

    currentPage++;
    window.location = window.location.pathname.substr(0, window.location.pathname.lastIndexOf("#")) + "#page=" + (currentPage);
    displayPage();
        
}

//Ges to previous page
function prevPage() {
    if (pages.length > 0 && (currentPage <= 1)) {return; }

    currentPage--;
     window.location = window.location.pathname.substr(0, window.location.pathname.lastIndexOf("#")) + "#page=" + (currentPage);
    displayPage();
        
}



/**
* Using filter to return all movies matching all criterias.
* movies - an array of movies
* criterias - an object of criterias, using same keys as movies object
* return - movies matching filter object
*/
function find(movies, criterias) {
    return movies.filter(function(movie) {
        if (new RegExp(criterias.genre, 'gi').test(JSON.stringify(genres_object[movie.id]))) {
            return Object.keys(criterias).every(function(c) {
                //Check object criteria with movie property
                if (c === 'genre'){return true;} //override genre
                return new RegExp(criterias[c], 'gi').test(movie[c]);
            });
        } else {
            //return false match of genres.
            return false;
        }
    });
}

function search(title, genre, year, actor, director, country) {
    var results = [];
    if (!title && !genre && !year && !actor && !director && !country) {
        setPageComplete(true);
        return;
    }

    //Not the best way to set the title here :), but will work for now//
    setPageTitle("Search - " + genre + " " + title + " " + year + " " + actor + " " + director + " " + country);

    //Use an object containing search to filter out movies from the json Object
    results = find(convertToArray(movies_object), {
        otitle: title,
        year: year,
        folk: actor,
        dir: director,
        country: country,
        genre: genre
    });


  return results;
}







window.onload = function () {
    
    var query_params = get_query_string_parameters(), //Get parameters from uri
                page = query_params.page,
                 sTitle, sGenre, sYear, sActor, sDirector, sCountry;

    if (query_params.title) {
        sTitle = query_params.title;
        document.querySelector("#search-string").innerHTML = sTitle;
    }
    if (query_params.year) {
        sYear = query_params.year;
        document.querySelector("#search-string").innerHTML += " " + sYear;
    }

    if (query_params.actor) {
        sActor = query_params.actor;
        document.querySelector("#search-string").innerHTML += " " + sActor;
    }

    if (query_params.director) {
        sDirector = query_params.director;
        document.querySelector("#search-string").innerHTML += " " + sDirector;
    }

    if (query_params.genre) {
        sGenre = query_params.genre;
        document.querySelector("#search-string").innerHTML += " " + sGenre;
    }

    if (query_params.country) {
        sCountry = query_params.country;
        document.querySelector("#search-string").innerHTML += " " + sCountry;
    }
        
    //Check if page parameter set, if not set current page to 1
    if (page) {
        currentPage = page;
    } else {
        //Default page
        currentPage = 1;
        page = currentPage;
    }
    
    //Create pages from the array of objects.
    //Only if the pages does not already exist.
    if (pages.length <= 0) {
         //Run the pages creator.
        results = search(sTitle, sGenre, sYear, sActor, sDirector, sCountry);

        createPages(results, 15);
    }
    //Set button listeners
    button_next.onclick = function () { nextPage(); };
    button_prev.onclick = function () { prevPage(); };
    

    //Display page
    displayPage();

}

