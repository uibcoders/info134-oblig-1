/**
 * Created by sto020 on 08.03.2017.
 */

var page_title = document.querySelector("#page-title");

function setPageTitle(title)
{
    page_title.innerHTML = "Filmbiblioteket - " + title;
}

function getMovieTrailer(movie) {
    var trailer = movie['youtube trailer id'];
    if (!trailer || trailer.length <= 1) {
        trailer = 'https://www.youtube.com/results?search_query=' + movie.otitle.split(" ").join("+") + "+" + movie.year + "+" + 'trailer';
    } else {
        trailer = 'https://www.youtube.com/watch?v=' + trailer.trim();    
    }
    return trailer;
    
}

/* Gets genres for a movie.
*  Sets to default if no genres
*  movie - The movie to get the genres from.
*/
function getMovieGenres(movie) {
    var genres = genres_object[movie.id];
    if (typeof genres === 'undefined') {
        genres = ["Ingen Data!"];
    }
    return genres;
}

/* Gets reviews for a movie.
*  movie - The movie to get the reviews from.
*/
function getMovieReviews(movie) {
    var reviews = reviews_object[movie.id],
        reviews_arr = [];
    if (typeof reviews === 'undefined') {
    } else {
        for (var rID in reviews) {
            reviews_arr.push(reviews[rID])
        }
    }
    return reviews_arr;
}

function getAverageRating(movie) {
    var reviews = getMovieReviews(movie),
        avg = 0;
    if (reviews.length > 0) {
        for (var i in reviews) {
            avg += reviews[i].rating;
        }
        avg = (avg / reviews.length).toFixed(1)
    }

    return avg;
}

/* Gets actors for a movie.
*  Sets to default if no actors
*  movie - The movie to get the actors from.
*/
function getMovieActors(movie) {
    var actors = movie.folk;
    if (!actors || actors.length <= 1){
        actors = "Ingen skuespillere funnet!";
    }
    return actors;
}

/* Gets directors for a movie.
*  Sets to default if no directors
*  movie - The movie to get the directors from.
*/
function getMovieDirectors(movie) {
    var directors = movie.dir;
    if (!directors || directors.length <= 1){
        directors = "Ingen regissører funnet!";
    }
    return directors;
}

/* Gets description for a movie.
*  Sets to default if no description
*  movie - The movie to get the description from.
*/
function getMovieDescription(movie) {
    var description = movie.description;
    if (!description || description.length <= 1){
        description = "Ingen  beskrivelse funnet!";
    }
    return description;
}

/* Checks if data is not empty and larger than 1.
* data - the data to check.
*/
function checkData(data) {
    if (!data || data.length <= 1){
        data = "Ingen Data";
    }
    return data; 
}

/*Get movie poster uri from movie id.
* id - the movie id.
*/
function getMoviePoster(id) {

    var length = id.toString().length;
    var key = "0";
    if (length > 3) {
        key = id.toString().substr(0, 1);
    }

    return 'https://nelson.uib.no/o/' + key + '/' + id + '.jpg';

}

/*Because some titles have the first word in the end
* separated by a comma. We change the order by splitting it.
*/
function refactorTitle(title)
{
    //Split text if comma is followed by a captial letter. Avoiding titles with valid commas in it.
    var temp = title.split(/, (?=[A-Z])/);
    
    //Refactor only if split in two. Just in case, else do nothing to the title.
    if(temp.length == 2) {
    //Return refactored title.
    return temp[1] + " " + temp[0];
    }
    //Return original
    return title;
}

function getRootUrl(){
    var path = window.location.pathname,
        url = path.substr(0, path.lastIndexOf("/")),
        sub = url.substr(url.lastIndexOf("/"),url.length);
    
    if (sub == "/pages"){
          url = url.substr(0, url.lastIndexOf("/"));  
        }
    return url;
}

/*Converts a json object to an array
* mObject - The object to be converted -eg. movies_object
* return - a  array of movie objects. 
*/
function convertToArray(mObject) {
    var arr = [];//an array
    for (var reg in mObject) {
        arr.push(mObject[reg]); // add to array
    }
    return arr;
}

/*Get a random object from an array
* arr - An array
*/
function pickRrandomFromArray(arr) {
return arr[Math.floor(Math.random() * arr.length)];
}




