/**
 * Created by sto020, can013, jmo034
 */


/*Function to handle all image errors*/
/*Sets a default image source if error*/
function onImageError(e) {
    e.onerror = null;
    console.warn("Error loading image from server(" + e.src + "), using default! " + getRootUrl()  + "/assets/images/default.jpg");
    e.src = getRootUrl() + "/assets/images/default.jpg";
}


function imageLoaded(e) {
    console.log("Image Loaded: " + e.src);
    e.classList.remove("img-loading");
  }

/**
* testing modal view, incomplete
**/
function showModal(movieID) {
    var modal = document.querySelector("#movie-modal");
    var close_modal = document.querySelector("#close");
    var modal_c = document.querySelector("#modal-content")
    
    var movie_display = [];
    movie_display.push(movies_object[movieID]);
    createMovieModal(modal_c,movie_display);
    modal.style.display = "block";
    
    close_modal.onclick = function() { modal.style.display = "none";};
    
}

/* Shows the search field at index page*/
function showSearchField() {
    var searchBox = document.querySelector("#search-box");
    searchBox.style.display = "block";
    searchBox.focus();
    
}
/* Starts the search after hitting enter by title at index page*/
function textSearch(event) {
    var input = document.querySelector("#search-box").value,
        result = getRootUrl() + "/pages/search.html?title=" + input;
    
    if (event.keyCode === 13) {
        window.location.href = result;
    }
}


/* Function for showing and hiding the advanced search form */
function showAdvancedSearch() {
    advDiv = document.getElementById("formStyleId");
    advBtn = document.getElementById("advancedSearchBtn");
    if(advDiv.style.display !== "none") {
        if(advBtn.value !== "Advanced Search") {
            advBtn.value = "Advanced Search";
        }
        advDiv.style.display = "none";
    } else {
        advBtn.value = "Hide";
        advDiv.style.display = "block";
    }
}

/* Function to search by input after hitting the search button in advanced search.*/
function advancedSearch() {
    var actorInput = document.querySelector("#textFieldActor").value, // load of variables.
        directorInput = document.querySelector("#textFieldDirector").value,
        yearInput = document.querySelector("#textFieldYear").value,
        countryInput = document.querySelector("#textFieldCountry").value,
        genreInput = document.querySelector("#genreSelect").value,
        result = getRootUrl() + "/pages/search.html?",
        searchString = "";
        
        
    if(actorInput) {
        searchString += "actor=" + actorInput;
    }
    if(directorInput) {
        if(actorInput) { // The first item in html won't have '&' infront so it needs to see if the first item has already been input. 
            searchString += "&director=" + directorInput;
        } else { // otherwise this will be the first item excluding the '&'. 
            searchString += "director=" + directorInput;
        }
    }
    if(yearInput) {
        if(actorInput || directorInput) {
            searchString += "&year=" + yearInput;
        } else {
            searchString += "year=" + yearInput;
        }
    }
    
    if(countryInput) {
        if(actorInput || directorInput || yearInput) {
            searchString += "&country=" + countryInput;
        } else {
            searchString += "country=" + countryInput;
        }
    }
    
    if(genreInput) {
        if(actorInput || directorInput || yearInput || countryInput) {
            searchString += "&genre=" + genreInput;
        } else {
            searchString += "genre=" + genreInput;
        }
    }
    
    if (actorInput || directorInput || yearInput || countryInput || genreInput) { // Checks if at least one field has received an input from user.
        window.location.href = result + searchString;
    }
}

