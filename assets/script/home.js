/**
 * Created by sto020 on 12.03.2017.
 */



window.onload = function () {
    setPageTitle("Hjem");
    var movies = convertToArray(movies_object),//Array of sorted movies
        mostRecent = movies.slice((movies.length - 10), movies.length).reverse(), //Array of recent movies, sliced from movies.
        returnedMovies = [],//Array for returned movies
        recommenedMovies = []; //Array for recommended movies
    

    //Get 10* 2 random movies and put them in arrays.
    for (i = 0; i < 10; i++) {
        returnedMovies.push(pickRrandomFromArray(movies));
        recommenedMovies.push(pickRrandomFromArray(movies));
    }
    
    //Create all content from the above arrays.
    createMovieThumbnails(document.querySelector("#last-returned-thumbnails"), returnedMovies);
    createMovieThumbnails(document.querySelector("#recommended-movies-thumbnails"), recommenedMovies);
    createMoviePosters(document.querySelector("#movies-main"),mostRecent);

    
}
