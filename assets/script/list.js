/**
 * Created by sto020, jmo034 on - 12.03.2017.
 */
var pages = [],
    currentPage,
    button_next = document.querySelector("#btn-next"),
    button_prev = document.querySelector("#btn-prev"),
    text_pages = document.querySelector("#text-pages");

/*Create arrays within a new array with a specified size.
* Used for pagination
* arr   - Input array.
* cSize - The sizes of the child arrays.
*/
function createPages(arr, cSize) {
    if (pages.length > 0) {return pages; }
    var i;
    for (i = 0; i < arr.length; i += cSize) {
        pages.push(arr.slice(i, i + cSize));
    }
    return pages;
}

//Creates and displays data on the page
//Uses currentPage as 
function displayPage() {
    setPageTitle("All movies - Page " + currentPage + " of " + pages.length);

    if (pages.length > 0 && (currentPage <= (pages.length)) && currentPage >= 1) {
        text_pages.innerHTML = "Page " + currentPage + " of " + pages.length;
        createMoviePosters(document.querySelector("#movies-main"), pages[currentPage - 1]);
        //createMovieThumbnails(document.querySelector("#movies-main"),pages[currentPage-1]);
    }
    
}


//Goes to the next page 
function nextPage() {
    if (pages.length > 0 && (currentPage >= (pages.length))) {return; }

    currentPage++;
     window.location = window.location.pathname.substr(0, window.location.pathname.lastIndexOf("#")) + "#page=" + (currentPage);
    displayPage();
        
}

//Ges to previous page
function prevPage() {
    if (pages.length > 0 && (currentPage <= 1)) {return; }

    currentPage--;
     window.location = window.location.pathname.substr(0, window.location.pathname.lastIndexOf("#")) + "#page=" + (currentPage);
    displayPage();
        
}


//On window load
window.onload = function () {
    var query_params = get_query_string_parameters(),
        page = query_params.page;
    
    //Check if page parameter set, if not set current page to 1
    if (page) {
        currentPage = page;
    } else {
        //Default page
        currentPage = 1;
        page = currentPage;
    }
    
    //Create pages from the array of objects.
    //Only if the pages does not already exist.
    if (pages.length <= 0) {
         //Run the pages creator.
        createPages(convertToArray(movies_object), 40);
    }
    
    //Set button listeners
    button_next.onclick = function () { nextPage(); };
    button_prev.onclick = function () { prevPage(); };
    

    //Display page
    displayPage();

};